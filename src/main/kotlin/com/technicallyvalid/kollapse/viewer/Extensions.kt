/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse.viewer

import javafx.scene.image.Image
import javafx.scene.image.PixelFormat
import javafx.scene.image.WritableImage

fun IntArray.toTwoDimensions(width: Int): Array<IntArray> {
    require(size % width == 0) { "size ($size) not divisible by width ($width)" }
    return Array(size / width) { i -> IntArray(width) { j -> this[j + width * i] }}
}

fun Array<IntArray>.toOneDimension(): IntArray {
    val width = this[0].size
    return IntArray(width * size) { i -> this[i / width][i % width] }
}

fun Image.zoom(factor: Int): Image {
    if (factor <= 0) throw IllegalArgumentException("factor must be positive: $factor")
    if (factor == 1) return this
    val w = width.toInt()
    val h = height.toInt()
    val newImage = WritableImage(w * factor, h * factor)
    val components = 3
    val buffer = ByteArray(w * h * factor * factor * components)
    var i = 0

    // This could be done more efficiently, using pixelReader.getPixels, but it makes like...a 20ms difference, but 4x
    // as much code. Not worth it.
    for (y in 0 until h * factor) for (x in 0 until w * factor) {
        val color = pixelReader.getColor(x / factor, y / factor)
        buffer[i++] = (color.red * 255).toByte()
        buffer[i++] = (color.green * 255).toByte()
        buffer[i++] = (color.blue * 255).toByte()
    }
    newImage.pixelWriter.setPixels(0, 0, w * factor, h * factor, PixelFormat.getByteRgbInstance(), buffer, 0, w * factor * components)
    return newImage
}