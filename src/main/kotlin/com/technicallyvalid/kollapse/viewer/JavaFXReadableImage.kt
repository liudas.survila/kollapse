/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse.viewer

import com.technicallyvalid.kollapse.ReadableImage
import javafx.scene.image.Image

class JavaFXReadableImage(override val image: Image) : ReadableImage<Image, JavaFXColor> {
    override val width = image.width.toInt()
    override val height = image.height.toInt()

    override fun getPixel(x: Int, y: Int): JavaFXColor {
        return JavaFXColor(image.pixelReader.getColor(x, y))
    }
}