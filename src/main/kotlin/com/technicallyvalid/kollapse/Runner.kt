/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

package com.technicallyvalid.kollapse

class Runner(private val runnerParameters: RunnerParameters) {
    fun run(model: Model): List<ModelResult> {
        return (0 until runnerParameters.screenshots).map { i ->
            val success = (0 until runnerParameters.maxRetries).asSequence().map {
                val limit = if (runnerParameters.limit >= 1) runnerParameters.limit else Int.MAX_VALUE
                model.run(runnerParameters.random.nextLong(), limit)
            }.any { it }
            if (success) SuccessfulModelResult(model) else FailedModelResult(model)
        }
    }
}