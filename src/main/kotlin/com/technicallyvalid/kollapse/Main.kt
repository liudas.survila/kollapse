/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

package com.technicallyvalid.kollapse

import java.io.File
import java.nio.file.Files
import java.nio.file.Paths
import java.time.Duration
import java.time.Instant
import java.util.*
import javax.imageio.ImageIO

object Main {
    @JvmStatic
    fun main(args: Array<String>) {
//        run()
        runCircuit()
    }

    private fun runCircuit() {
        val circuit = SimpleTiledModelSampleParameters("Circuit")
        circuit.width = 34
        circuit.height = 34
        circuit.periodic = true
        circuit.black = false
        circuit.maxRetries = 3
        runSimpleTiledModel(1, circuit)
    }

    private fun run() {
        val samplesPath = Paths.get("WaveFunctionCollapse/samples.xml")
        if (!Files.exists(samplesPath)) {
            System.err.println("Could not find $samplesPath on disk. Perhaps you need to check out the WaveFunctionCollapse submodule (run git submodule init && git submodule update).")
            System.exit(1)
        }
        val samples = SamplesXmlParser(Paths.get("WaveFunctionCollapse/samples.xml")).parse()
        Files.createDirectories(Paths.get("out"))
        val mainStart = Instant.now()
        samples.forEachIndexed { counter, def ->
            if (def is SimpleTiledModelSampleParameters) {
                runSimpleTiledModel(counter, def)
            } else if (def is OverlappingModelSampleParameters) {
                runOverlappingModel(counter, def)
            }
        }
        val mainFinishedTime = Instant.now()
        println("Grand total: ${Duration.between(mainStart, mainFinishedTime).seconds} seconds")
    }

    private fun runSimpleTiledModel(counter: Int, def: SimpleTiledModelSampleParameters) {
        val name = def.name
        val params = XmlLoader().parseFile(Paths.get("WaveFunctionCollapse/samples/$name/data.xml"))
        params.apply {
            tileToFile = { tile -> Paths.get("WaveFunctionCollapse/samples/$name/${tile.name}.png") }
            tileCardToFile = { tile, i -> Paths.get("WaveFunctionCollapse/samples/$name/${tile.name} $i.png") }
            validate()
        }
        val model = SimpleTiledModel(params, def)

//        val results = Runner(def).run(model)
        for (i in 0 until def.screenshots) {
            for (retryCount in 1..10) {
                val seed = Random().nextLong()
                val start = Instant.now()
                val limit = if (def.limit < 1) Int.MAX_VALUE else def.limit
                val finished = model.run(seed, limit)
                println("$name $i ($retryCount):")
                if (finished) {
                    val finishedTime = Instant.now()
                    println(" Took ${Duration.between(start, finishedTime).toMillis()}ms")
                    val image = model.renderTo(AwtRenderer)
                    ImageIO.write(image, "png", File("out/$counter $name $i.png"))
                    println(" DONE")
                    break // stop retrying
                } else {
                    System.err.println(" CONTRADICTION, retry #$retryCount")
                }
            }
        }
    }

    private fun runOverlappingModel(counter: Int, def: OverlappingModelSampleParameters) {
        val name = def.name
        val params = try {
            OverlappingModelParameters(ImageIO.read(File("WaveFunctionCollapse/samples/$name.png")))
        } catch (e: Exception) {
            throw RuntimeException("Failed while trying to read $name.png!", e)
        }
        val model = OverlappingModel(params, def)
        val r = Random()
//        Runner(def).run(model)
        screenshot@ for (i in 0 until def.screenshots) {
            for (retryCount in 1..10) {
                val seed = r.nextLong()
                val start = Instant.now()
                val limit = if (def.limit < 1) Int.MAX_VALUE else def.limit
                val finished = model.run(seed, limit)
                println("$name $i ($retryCount):")
                if (finished) {
                    val finishedTime = Instant.now()
                    println(" Took ${Duration.between(start, finishedTime).toMillis()}ms")
                    val image = model.renderTo(AwtRenderer)
                    ImageIO.write(image, "png", File("out/$counter $name $i.png"))
                    println(" DONE")
                    break@screenshot // stop retrying
                } else {
                    System.err.println(" CONTRADICTION, retry #$retryCount")
                }
            }
            System.err.println("FAILED, continuing anyway")
        }
    }
}

internal fun kotlinSimpleModelCircles(): SimpleTiledModelParameters {
    return SimpleTiledModelParameters().apply {
        tileSize = 32
        tileToFile = { tile -> Paths.get("WaveFunctionCollapse/samples/Circles/${tile.name}.png") }
        tiles = listOf(
                Tile(name = "b_half", symmetry = "T"),
                Tile(name = "b_i", symmetry = "I"),
                Tile(name = "b_quarter", symmetry = "L"),
                Tile(name = "w_half", symmetry = "T"),
                Tile(name = "w_i", symmetry = "I"),
                Tile(name = "w_quarter", symmetry = "L"),
                Tile(name = "b", symmetry = "X"),
                Tile(name = "w", symmetry = "X")
        )
        neighbors = listOf(
                Neighbor("b_half", "b_half"),
                Neighbor("b_half", 1, "b_half", 3),
                Neighbor("b_half", 3, "b_half", 1),
                Neighbor("b_half", "b_half", 3),
                Neighbor("b_half", "b_half", 2),
                Neighbor("b_half", "b_i"),
                Neighbor("b_half", 3, "b_i", 3),
                Neighbor("b_half", 1, "b_i"),
                Neighbor("b_half", "b_quarter"),
                Neighbor("b_half", 1, "b_quarter"),
                Neighbor("b_half", 2, "b_quarter"),
                Neighbor("b_half", 3, "b_quarter", 1),
                Neighbor("b_i", "b_i"),
                Neighbor("b_i", 1, "b_i", 1),
                Neighbor("b_i", "b_quarter"),
                Neighbor("b_i", 1, "b_quarter", 1),
                Neighbor("b_quarter", "b_quarter", 1),
                Neighbor("b_quarter", 1, "b_quarter"),
                Neighbor("b_quarter", 2, "b_quarter"),
                Neighbor("b_quarter", "b_quarter", 2),
                Neighbor("b_half", 1, "w_half", 1),
                Neighbor("b_half", "w_half", 1),
                Neighbor("b_half", 3, "w_half"),
                Neighbor("b_half", 3, "w_half", 3),
                Neighbor("b_half", "w_i", 1),
                Neighbor("b_half", 1, "w_i", 1),
                Neighbor("b_half", 3, "w_i"),
                Neighbor("b_half", "w_quarter", 1),
                Neighbor("b_half", "w_quarter", 2),
                Neighbor("b_half", 1, "w_quarter", 1),
                Neighbor("b_half", 3, "w_quarter"),
                Neighbor("b_i", "w_half", 1),
                Neighbor("b_i", 1, "w_half"),
                Neighbor("b_i", 1, "w_half", 3),
                Neighbor("b_i", "w_i", 1),
                Neighbor("b_i", 1, "w_i"),
                Neighbor("b_i", "w_quarter", 1),
                Neighbor("b_i", 1, "w_quarter"),
                Neighbor("b_quarter", "w_half"),
                Neighbor("b_quarter", "w_half", 3),
                Neighbor("b_quarter", "w_half", 2),
                Neighbor("b_quarter", 1, "w_half", 1),
                Neighbor("b_quarter", "w_i"),
                Neighbor("b_quarter", 1, "w_i", 1),
                Neighbor("b_quarter", "w_quarter"),
                Neighbor("b_quarter", "w_quarter", 3),
                Neighbor("b_quarter", 1, "w_quarter", 1),
                Neighbor("b_quarter", 1, "w_quarter", 2),
                Neighbor("w_half", "w_half"),
                Neighbor("w_half", 1, "w_half", 3),
                Neighbor("w_half", 3, "w_half", 1),
                Neighbor("w_half", "w_half", 3),
                Neighbor("w_half", "w_half", 2),
                Neighbor("w_half", "w_i"),
                Neighbor("w_half", 3, "w_i", 3),
                Neighbor("w_half", 1, "w_i"),
                Neighbor("w_half", "w_quarter"),
                Neighbor("w_half", 1, "w_quarter"),
                Neighbor("w_half", 2, "w_quarter"),
                Neighbor("w_half", 3, "w_quarter", 1),
                Neighbor("w_i", "w_i"),
                Neighbor("w_i", 1, "w_i", 1),
                Neighbor("w_i", "w_quarter"),
                Neighbor("w_i", 1, "w_quarter", 1),
                Neighbor("w_quarter", "w_quarter", 1),
                Neighbor("w_quarter", 1, "w_quarter"),
                Neighbor("w_quarter", 2, "w_quarter"),
                Neighbor("w_quarter", "w_quarter", 2),
                Neighbor("b", "b"),
                Neighbor("b", "b_half", 1),
                Neighbor("b", "b_i", 1),
                Neighbor("b", "b_quarter", 1),
                Neighbor("b", "w_half"),
                Neighbor("b", "w_half", 3),
                Neighbor("b", "w_i"),
                Neighbor("b", "w_quarter"),
                Neighbor("w", "w"),
                Neighbor("w", "w_half", 1),
                Neighbor("w", "w_i", 1),
                Neighbor("w", "w_quarter", 1),
                Neighbor("w", "b_half"),
                Neighbor("w", "b_half", 3),
                Neighbor("w", "b_i"),
                Neighbor("w", "b_quarter")
        )
        subsets = mapOf(
                Pair("Large", Subset(listOf(
                        Tile("b_quarter"),
                        Tile("w_quarter")
                ))),
                Pair("More", Subset(listOf(
                        Tile("b_quarter"),
                        Tile("w_quarter"),
                        Tile("b"),
                        Tile("w")
                ))),
                Pair("Without", Subset(listOf(
                        Tile("b_half"),
                        Tile("b_i"),
                        Tile("b_quarter"),
                        Tile("w_i"),
                        Tile("w_quarter")
                )))
        )
    }
}