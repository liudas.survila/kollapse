/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

/*
Not sure exactly what I'm doing with these yet -- still very much in development.
 */

interface ModelResult {
    val success: Boolean
    val model: Model
}

class SuccessfulModelResult(override val model: Model) : ModelResult {
    override val success = true
}

class FailedModelResult(override val model: Model) : ModelResult {
    override val success = false
}