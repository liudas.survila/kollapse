/* Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/ */

package com.technicallyvalid.kollapse

import org.w3c.dom.Element
import org.w3c.dom.Node
import org.w3c.dom.NodeList
import javax.xml.xpath.XPath
import javax.xml.xpath.XPathConstants

/*
Various helpers for making Java's built-in XML parsing more bearable. Would rather not pull in a dependency just for
parsing samples.xml and data.xml.
 */

/**
 * Returns a list of all children that are of type [Node.ELEMENT_NODE].
 */
internal val Node.elementChildNodes: List<Node>
    get() = childNodes.asList().filter { it.nodeType == Node.ELEMENT_NODE }.toList()

/**
 * Get the attribute value off a node, or null if there's no attribute by the given name.
 *
 * @param name name of the attribute you retrieve
 * @receiver node to operate on
 * @return the value at the node, or null if the node had no such attribute (or can't have attributes)
 */
internal operator fun Node.get(name: String): String? {
    if (hasAttributes()) {
        val item = attributes.getNamedItem(name)
        if (item != null) {
            return item.textContent
        }
    }
    return null
}

internal fun NodeList.asList(): List<Node> = (0 until length).map { item(it) }

/**
 * Gets a node list for the given [xpath] on the current node.
 * @param xpath
 */
internal fun Element.nodeListByXPath(xpath: String, instance: XPath): NodeList = instance.evaluate(xpath, this, XPathConstants.NODESET) as NodeList
