/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package com.technicallyvalid.kollapse

import java.util.*

/**
 * Collection that stores a unique identifier (index) for each unique object (per HashMap conventions) provided to
 * the collection using [getOrMakeIndexForKey]. An index can be converted back into the object using [get].
 */
class IndexedMap<T> {
    private val lookup = HashMap<T, Int>()
    private val list = ArrayList<T>()

    /**
     * Gets or creates a new index for the given key.
     */
    fun getOrMakeIndexForKey(key: T): Int {
        val currentIdx = lookup[key]
        if (currentIdx != null) {
            return currentIdx
        }
        list.add(key)
        val newIdx = list.lastIndex
        lookup[key] = newIdx
        return newIdx
    }

    operator fun get(idx: Int) = list[idx]

    val size: Int
        get() = list.size
}