# Kollapse

Kollapse is a bitmap and tilemap generation tool written in [Kotlin](https://kotlinlang.org/) (for the JVM) based on the awesome work by [mxgmn](https://twitter.com/ExUtumno) on [WaveFunctionCollapse](https://github.com/mxgmn/WaveFunctionCollapse).

# Samples/Screenshots

For samples, check out the `WaveFunctionCollapse` repo linked above -- this repo is designed to generate the exact same samples as the original repo. The two-dimensional ones, at least.

# How can I run it?

Take a look at `Main.kt` -- it runs all the samples in a similar way to the reference implementation. Make sure you've [checked out the submodule first](https://git-scm.com/book/en/v2/Git-Tools-Submodules#Cloning-a-Project-with-Submodules) (`git submodule init && git submodule update`).

# How can I use this in my application?

To be honest, I don't recommend this yet. The API is still very much in flux and will be until it hits 1.0. But if you want, take a look at `Main.kt` and copy from it.

# Goals

The purpose of this repo is two-fold:

## Fully-functioning library

While mxgmn's work on WaveFunctionCollapse is great, and this repo surely wouldn't exist without it, it appears to be more of a showcase than a library that was intended to be reused.

Conversely, the goal of Kollapse is to be a library that can actually be used and embedded into applications, games, and build processes.

To be effective, it adheres to general industry best-practices: **readable, documented, tested, modular, performant[1], deterministic, and versioned**.

[1] The algorithm is not particularly fast -- runs that take over a second are quite common. So "performance" is relative here.

## JVM

The reference implementation is written in [C#](https://en.wikipedia.org/wiki/C_Sharp_(programming_language)) and runs on the [CLR](https://en.wikipedia.org/wiki/Common_Language_Runtime), which is of little use to those of us who prefer the [JVM](https://en.wikipedia.org/wiki/Java_virtual_machine). kollapse is written in [Kotlin](https://en.wikipedia.org/wiki/Kotlin_(programming_language)). Because most users of the JVM don't write in Kotlin, there's a possibility that the bytecode and APIs generated by Kotlin will be unpleasant to use in other JVM languages. To mitigate that possibility, there will be at least a couple unit tests written in Java, and development will keep in mind the [calling-Kotlin-from-Java](https://en.wikipedia.org/wiki/Kotlin_(programming_language)) use case.

# How does it work?

Great question! I don't actually know. The algorithm itself is way over my head, despite having ported every line over
to Kotlin one by one. That said, you don't really *need* to understand the algorithm in order to do that. So if you have
questions about how the algorithm works, you can ask me, but you'll likely have better luck with the original creator.

# Versioning

Kollapse adheres to [Semantic Versioning 2.0.0](http://semver.org/) (semver).

That said, semver is fairly underspecified when it comes to sub-1.0 versioning and actually documenting changes.

## Before 1.0

Any point releases (0.\*.**z**) *should* be binary-compatible with each other, provided the minor version is the same.

Any significant new features or API breakages will trigger a bump in the minor version (0.**y**.\*). Expect this to be fairly common -- this library is still under very active development.

After 1.0, the regular semver terms apply.

## Documenting changes

See CHANGELOG.md. Every major/minor/patch release will have notes, and each release will have a tag.

# Naming

It's the "collapse" from "WaveFormCollapse", with the `C` turned into `K` because it's Kotlin. Clever, right?

# Licensing

The "interesting" files are licensed under the MPL 2.0, while the test files are all public domain. All licensed files have a license at the top of each file. The full MPL (Mozilla Public License) is included at the root of this repository for convenience.

## MPL Links

The MPL is not quite as common as the others (GPL, MIT, etc), so the following links might help explain what it's about:

* [Mozilla Public License 2.0 (MPL-2.0) Explained in Plain English  - TLDRLegal](https://tldrlegal.com/license/mozilla-public-license-2.0-(mpl-2))
* [Mozilla Public License 2.0 - Choose a License](http://choosealicense.com/licenses/mpl-2.0/)
* [https://www.mozilla.org/en-US/MPL/2.0/FAQ/](https://www.mozilla.org/en-US/MPL/2.0/FAQ/)

### Boilerplate at the top of each file

One doesn't *have* to include the [boilerplate](https://www.mozilla.org/en-US/MPL/headers/) at the top of each file, but it's not that much, and it makes it easier to license files separately.
